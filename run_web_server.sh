python3 -m pip install -r requirements.txt
python3 db_setup.py
gunicorn core.web_server.web_server:app --workers 4 --worker-class uvicorn.workers.UvicornWorker # --bind 0.0.0.0:80
