import typing
import asyncio             # type: ignore
import mock                # type: ignore
import testing.postgresql  # type: ignore
import testing.redis       # type: ignore


def _run_postgres(function: typing.Callable) -> typing.Callable:
    def f(*args, **kwargs):  # type: ignore
        with testing.postgresql.Postgresql() as postgresql:
            url = postgresql.url().replace('postgresql', 'postgresql+asyncpg')
            print('Attached postgres:', url)
            with mock.patch("core.config.DATABASE_URL", url):
                from core.database import database
                loop = asyncio.get_event_loop()
                loop.run_until_complete(database.init_models())
                ret = function(*args, **kwargs)
        return ret
    return f


def _run_redis(function: typing.Callable) -> typing.Callable:
    def f(*args, **kwargs):  # type: ignore
        with testing.redis.RedisServer() as redis_server:
            dsn = redis_server.dsn()
            url = 'rediss://@{host}:{port}/{db}'.format(**dsn)
            print('Attached redis:', url)
            with mock.patch("core.config.MESSAGE_BROKER_URL", url):
                ret = function(*args, **kwargs)
        return ret
    return f


def mock_storages(function: typing.Callable) -> typing.Callable:
    return _run_postgres(_run_redis(function))
