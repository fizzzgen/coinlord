import ssl

from core import config


broker_url = config.MESSAGE_BROKER_URL
imports = ('core.engine.tasks.tasks',)
result_backend = config.MESSAGE_BROKER_URL
broker_use_ssl = {
    'ssl_cert_reqs': ssl.CERT_NONE
}
redis_backend_use_ssl = {
    'ssl_cert_reqs': ssl.CERT_NONE
}
beat_schedule = {
    "test-task": {
        "task": "core.engine.tasks.tasks.test",
        "schedule": 20.0,
    },
    "yobit-depth-task": {
        "task": "core.engine.tasks.tasks.yobit_depth",
        "schedule": 600.0,
    },
}

print("Celery config loaded")
