# -*- coding: utf-8 -*-
import attrdict  # type: ignore
import os        # type: ignore

DATABASE_URL = (os.environ.get("DATABASE_URL") or '').replace('postgres', 'postgresql+asyncpg')
MESSAGE_BROKER_URL = os.environ.get("REDIS_TLS_URL")
ENV = 'production'  # testing

config = attrdict.AttrDict()
config.twitter_accounts_file = "core/config/parsers/twitter_accounts.txt"
