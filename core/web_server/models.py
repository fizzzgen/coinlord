from pydantic import BaseModel


class TweetSchema(BaseModel):
    author: str
    text: str
    timestamp: int
