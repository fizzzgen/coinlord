# -*- coding: utf-8 -*-
import typing

from fastapi import FastAPI
from fastapi import Depends
from sqlalchemy.ext.asyncio import AsyncSession
from core.database import database as db
from core.database.queries import twitter
from core.web_server import models


app = FastAPI()


@app.get("/tweets/", response_model=typing.List[models.TweetSchema])
async def get_tweets(session: AsyncSession = Depends(db.get_session)) -> typing.List[models.TweetSchema]:

    tweets = await twitter.get_tweets(session)
    return [
        models.TweetSchema(
            author=tweet.author,
            text=tweet.text,
            timestamp=tweet.timestamp,
        ) for tweet in tweets
    ]


@app.post("/tweets/")
async def add_tweet(
    tweet: models.TweetSchema,
    session: AsyncSession = Depends(db.get_session),
) -> models.TweetSchema:

    added_tweet = twitter.add_tweet(session, tweet.author, tweet.text)
    db.safe_commit(session)
    return models.TweetSchema(
        author=added_tweet.author,
        text=added_tweet.text,
    )
