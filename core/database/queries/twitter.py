import typing
from sqlalchemy import select                    # type: ignore
from sqlalchemy.ext.asyncio import AsyncSession  # type: ignore
from core.database.database import Tweet


def convert_to_tweet(tweet: typing.Any) -> Tweet:
    return Tweet(author=tweet.author, text=tweet.text, timestamp=tweet.timestamp)


async def get_tweets(
    session: AsyncSession,
    author: typing.Optional[str] = None,
    from_timestamp: typing.Optional[int] = None,
) -> typing.List[Tweet]:
    query = select(Tweet)
    if author:
        query = query.filter(Tweet.author == author)
    if from_timestamp:
        query = query.filter(Tweet.timestamp >= from_timestamp)
    result = await session.execute(query)
    return [convert_to_tweet(tweet) for tweet in result]


def add_tweet(session: AsyncSession, author: str, text: str) -> Tweet:
    tweet = Tweet(author=author, text=text)
    session.add(tweet)
    return tweet
