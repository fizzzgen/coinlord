# -*- coding: utf-8 -*-
import logging
import typing

import sqlalchemy as sa                 # type: ignore
import time                             # type: ignore
from uuid import uuid4                  # type: ignore
from sqlalchemy.ext.declarative import declarative_base  # type: ignore
from sqlalchemy.ext.asyncio import AsyncSession          # type: ignore
from sqlalchemy.ext.asyncio import create_async_engine   # type: ignore
from sqlalchemy.orm import sessionmaker                  # type: ignore
from core import config

engine = create_async_engine(config.DATABASE_URL, echo=True)
Base = declarative_base()
async_session = sessionmaker(
    engine, class_=AsyncSession, expire_on_commit=False
)


def gen_key() -> str:
    return str(uuid4())


def gen_time() -> int:
    return int(time.time())


class Tweet(Base):
    __tablename__ = 'tweets'
    id = sa.Column(sa.String, primary_key=True, default=gen_key)
    author = sa.Column(sa.String)
    text = sa.Column(sa.String)
    timestamp = sa.Column(sa.Integer, default=gen_time)


class Depth(Base):
    __tablename__ = 'depths'
    id = sa.Column(sa.String, primary_key=True, default=gen_key)
    broker = sa.Column(sa.String)
    pair = sa.Column(sa.String)
    type = sa.Column(sa.String)
    price = sa.Column(sa.Float)
    amount = sa.Column(sa.Float)
    timestamp = sa.Column(sa.Integer)


class Trade(Base):
    __tablename__ = 'trades'
    id = sa.Column(sa.String, primary_key=True, default=gen_key)
    broker = sa.Column(sa.String)
    pair = sa.Column(sa.String)
    type = sa.Column(sa.String)
    price = sa.Column(sa.Float)
    amount = sa.Column(sa.Float)
    timestamp = sa.Column(sa.Integer)


async def init_models() -> None:
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


async def get_session() -> typing.AsyncGenerator[AsyncSession, None]:
    async with async_session() as session:
        yield session


async def safe_commit(session: AsyncSession) -> None:
    try:
        await session.commit()
    except Exception:
        logging.exception('Transaction exception')
        await session.rollback()
        raise
