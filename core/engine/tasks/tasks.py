import asyncio
from celery import Celery  # type: ignore
from core.engine.tasks import yobit

print("Loading tasks")
app = Celery()


@app.task
def test() -> None:
    print('test')


@app.task
async def yobit_depth() -> None:
    asyncio.run(yobit.get_and_save_depth())
