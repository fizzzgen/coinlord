import typing    # type: ignore
import time      # type: ignore
import aiohttp   # type: ignore
import asyncio   # type: ignore

from core.database import database

PAIRS_PER_REQUEST = 20


async def get(url: str):  # type: ignore
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as resp:
            return await resp.json(content_type=None)


async def get_depth_impl(pairs: typing.List[str]) -> typing.Dict[str, typing.Dict[str, typing.List[typing.List[float]]]]:  # noqa
    url = "https://yobit.net/api/3/depth/{}".format('-'.join(pairs))
    depth = await get(url)
    return depth


async def get_pairs() -> typing.List[str]:
    url = "https://yobit.net/api/3/info"
    resp = await get(url)
    pairs = resp["pairs"]
    return list(pairs.keys())


async def get_and_save_depth(n_iter: int = 3600) -> None:
    pairs = await get_pairs()
    print("fffff")
    async with database.async_session() as session:
        for i in range(0, min(n_iter * PAIRS_PER_REQUEST, len(pairs)), PAIRS_PER_REQUEST):
            depth = await get_depth_impl(pairs[i: i + PAIRS_PER_REQUEST])
            for pair in depth.keys():
                timestamp = int(time.time())
                for type in depth[pair].keys():
                    for d in depth[pair][type]:
                        row = database.Depth(
                            broker="yobit",
                            pair=pair,
                            type=type,
                            price=d[0],
                            amount=d[1],
                            timestamp=timestamp,
                        )
                        session.add(row)
            await database.safe_commit(session)
            await asyncio.sleep(1)
