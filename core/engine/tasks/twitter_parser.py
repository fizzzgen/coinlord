# -*- coding: utf-8 -*-

import typing
import time
import logging

from selenium import webdriver                         # type: ignore
from bs4 import BeautifulSoup                          # type: ignore
from selenium.webdriver.chrome.options import Options  # type: ignore


def get_pretty_html(twitter_username: str) -> str:
    chrome_options = Options()
    browser = webdriver.Chrome(options=chrome_options)
    browser.implicitly_wait(10)
    browser.get('https://twitter.com/{}/'.format(twitter_username))
    elem = browser.find_element_by_tag_name("body")
    browser.execute_script("window.scrollBy(0, window.innerHeight/2);")
    time.sleep(3)
    soup = elem.get_attribute('innerHTML')
    soup = BeautifulSoup(soup)
    soup = soup.prettify()
    browser.close()
    return soup


def get_posts_from(twitter_username: str) -> typing.List[str]:
    logging.info('Parsing posts from %s', twitter_username)
    texts: typing.Set[str] = set()
    for attempt in range(3):
        try:
            soup = get_pretty_html(twitter_username)
            opened = -10**9
            text: typing.List[str] = []
            for line in soup.split('\n'):
                if 'lang=' in line:
                    opened = 1
                    text = []
                elif line.replace(' ', '').startswith('</'):
                    opened -= 1
                    if opened == 0:
                        tweet = ' '.join(text)
                        while '  ' in tweet:
                            tweet = tweet.replace('  ', ' ')
                        if tweet.startswith(' '):
                            tweet = tweet[1:]
                        texts.add(tweet)
                        opened = -10**9
                elif line.replace(' ', '').startswith('<') and opened >= 1:
                    opened += 1
                elif opened >= 1:
                    text.append(line)
            break
        except Exception as ex:
            if attempt < 2:
                logging.exception(ex)
            else:
                raise
    logging.info('Parsed posts from %s: %s', twitter_username, len(texts))
    return list(texts)
