from core.engine.tasks import twitter_parser


def test_parsing():  # type: ignore
    def get_pretty_html_mocked(login: str) -> str:
        return '''
        <p lang="eng">
          <span>
            tweet part 1
          </span>
          <span>
            tweet part 2
          </span>
        </p>
        <p>
          some another text on page
        </p>
        <span>
        </span>
        '''
    twitter_parser.get_pretty_html = get_pretty_html_mocked
    parsed = twitter_parser.get_posts_from('test_login')
    assert parsed == ["tweet part 1 tweet part 2", ]
