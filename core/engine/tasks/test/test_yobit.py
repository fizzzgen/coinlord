import pytest            # type: ignore
from test import mocked  # type: ignore


@pytest.mark.asyncio
@mocked.mock_storages
async def test_yobit_depth():  # type: ignore
    from core.engine.tasks import yobit
    pairs = await yobit.get_pairs()
    pairs = pairs[:yobit.PAIRS_PER_REQUEST]
    depth = await yobit.get_depth_impl(pairs)
    assert list(depth.keys()) == pairs
    for pair in pairs:
        for key in depth[pair].keys():
            assert key in ['asks', 'bids']
            if depth[pair][key]:
                assert len(depth[pair][key][0]) == 2
    await yobit.get_and_save_depth(2)
