import asyncio
import logging

from logger import setup_logging
from core.database import database


if __name__ == "__main__":
    setup_logging.setup_logging()
    asyncio.run(database.init_models())
    logging.info("Db models initialized")
