python3 -m pip install -r requirements.txt
python3 -m flake8 --max-line-length=120 .
python3 -m mypy --disallow-untyped-defs .
python3 -m pytest .